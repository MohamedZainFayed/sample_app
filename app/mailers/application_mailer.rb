class ApplicationMailer < ActionMailer::Base
  default from: 'mohamedzain@acm.org'
  layout 'mailer'
end
